package flinktableapi;

import flinktableapi.UserDataTypes.*;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.BatchTableEnvironment;
import org.apache.flink.types.Row;


/**
 * Vorlesung Datenbanksysteme
 * Uebungsblaetter der 1. Woche
 * Aufgaben zu Apache Flink
 */
public class AufgabeTableAPI {

    /**
     * Die folgenden Tabellen entsprechen dem Schema des Uebungszettels
     */
    private static Table teile;
    private static Table lieferanten;
    private static Table projekte;
    private static Table lieferungen;
    private static Table zubehoer;

    private static ExecutionEnvironment env;
    private static BatchTableEnvironment tEnv;

    public static void main(String[] args) throws Exception {

        initialize();

        //////////
        System.out.println("Beispiel: Welche Teile besitzen die Farbe Schwarz und wurden an ein Marburger Projekt geliefert?");
        //////////
        Table beispiel = teile.filter("Farbe = 'Schwarz'")
                .join(lieferungen.as("LNr, lTNr, lPNr, Menge"))
                .where("TNr = lTNr")
                .join(projekte)
                .where("lPNr = PNr && Ort = 'Marburg'")
                .select("TNr, TName, Farbe, Gewicht");

        // Konvertieren des Tables 'filtered' in generisches DataSet.
        // Wichtig: Das Schema muss mit der gewaehlten Klasse übereinstimmen!
        DataSet<Teil> ergebnisBeispiel = tEnv.toDataSet(beispiel, Teil.class);

        // Ausgabe es Ergebnisses
        ergebnisBeispiel.print();

        //////////
        // Praesenzuebung:
        //////////

//        //////////
        System.out.println();
        System.out.println("Ermitteln Sie alle schwarzen Teile, die schwerer sind als 30 kg.");
        Table pa1 = teile.filter("Farbe = 'Schwarz' && Gewicht > 30");
        DataSet<Teil> pa1aResult = tEnv.toDataSet(pa1, Teil.class);
        // Ausgabe es Ergebnisses
        pa1aResult.print();
//        //////////


//        //////////
        System.out.println();
        System.out.println("Wie heissen die Projekte aus Marburg?");
        Table pa1b = projekte.filter("Ort = 'Marburg'");
        tEnv.toDataSet(pa1b, Projekt.class).print();
//        //////////


//        //////////
        System.out.println();
        System.out.println("An welchen Orten sind Lieferanten oder Projekte ansaessig?");
        Table pa1c = lieferanten.select("Ort")
                .union(projekte.select("Ort"));
        tEnv.toDataSet(pa1c, Row.class).print();

//        //////////


//        //////////
        System.out.println();
        System.out.println("Welches Teil ist kein Zubehoer und wiegt mehr als 10 Kilo?");
        Table pa1d = teile.minus(zubehoer)
                .filter("Gewicht > 10");
        tEnv.toDataSet(pa1d,Teil.class).print();
//        //////////


        //////////
        // Uebungsblatt:
        //////////

//        //////////
        System.out.println();
        System.out.println("Ermitteln Sie alle blauen Teile, die weniger als 8kg wiegen.");
        Table ua2a = teile.union(zubehoer).filter("Farbe = 'Blau' && Gewicht < 8");
        tEnv.toDataSet(ua2a, Teil.class).print();
//        //////////


//        //////////
       System.out.println();
       System.out.println("Ermitteln Sie alle Namen von Projekten und Lieferanten " +
               "(die Ausgabe soll eine Relation mit einem Attribut Name sein).");

       Table ua2b = projekte.select("PName as Name").union(lieferanten.select("LName as Name")).select("Name");
       tEnv.toDataSet(ua2b, Row.class).print();
//        //////////


//        //////////
        System.out.println();
        System.out.println("Geben Sie die Teilnummern derjenigen Teile an, die in keiner Lieferung stehen.");
        Table ua2c = teile.select("TNr").minus(lieferungen.select("TNr"));
        tEnv.toDataSet(ua2c, Row.class).print();
//        //////////


//        //////////
        System.out.println();
        System.out.println("Wie ist die Projektnummer aller Projekte, die genau einmal beliefert wurden?");
        Table intersect = lieferungen.select("PNr").intersect(projekte.select("PNr"));
        Table minus = lieferungen.select("PNr").minusAll(intersect);
        Table ua2d = lieferungen.select("PNr").minus(minus);
        tEnv.toDataSet(ua2d, Row.class).print();
//        //////////


    }


    /**
     * Initialisieren und laden einfacher Testdaten
     */
    public static void initialize() {

        env = ExecutionEnvironment.createCollectionsEnvironment();
        tEnv = BatchTableEnvironment.create(env);

        // Initialisiere Teile
        teile = tEnv.fromDataSet(
                env.fromElements(
                        new Teil(101, "Schraube A", "Grau", 3),
                        new Teil(102, "Schraube A", "Schwarz", 3),
                        new Teil(103, "Schraube B", "Grau", 4),
                        new Teil(104, "Schraube B", "Schwarz", 4),
                        new Teil(121, "Blech", "Gruen", 50),
                        new Teil(122, "Blech", "Blau", 50),
                        new Teil(123, "Blech", "Gelb", 50),
                        new Teil(124, "Blech", "Rot", 50),
                        new Teil(131, "Rohr", "Schwarz", 50),
                        new Teil(132, "Rohr", "Gruen", 30),
                        new Teil(133, "Rohr", "Rot", 30),
                        new Teil(134, "Rohr", "Blau", 30),
                        new Teil(141, "Brett", "Braun", 25),
                        new Teil(142, "Brett", "Braun", 50),
                        new Teil(143, "Brett", "Braun", 75)
                ), "TNr, TName, Farbe, Gewicht");

        // Initialisiere Lieferanten
        lieferanten = tEnv.fromDataSet(
                env.fromElements(
                        new Lieferant(1, "Schrauben Meier", "Marburg"),
                        new Lieferant(2, "Fachhandel Schmitt", "Giessen"),
                        new Lieferant(3, "Eisenwaren Schrotti", "Frankfurt"),
                        new Lieferant(4, "Giesserei AG", "Frankfurt"),
                        new Lieferant(5, "Baumarkt Gmbh", "Kassel")
                ), "LNr, LName, Ort");

        // Initialisiere Projekte
        projekte = tEnv.fromDataSet(
                env.fromElements(
                        new Projekt(1, "Marburg Mall", "Marburg"),
                        new Projekt(2, "Flughafen BER", "Berlin"),
                        new Projekt(3, "Stuttgart 21", "Stuttgart"),
                        new Projekt(4, "Musterhaus", "Musterstadt"),
                        new Projekt(5, "Umwelttrasse Lahnberge", "Marburg")
                ), "PNr, PName, Ort");

        // Initialisiere Lieferungen
        lieferungen = tEnv.fromDataSet(
                env.fromElements(
                        new Lieferung(1, 102, 2, 100),
                        new Lieferung(1, 103, 1, 50),
                        new Lieferung(2, 114, 1, 20),
                        new Lieferung(5, 112, 3, 50),
                        new Lieferung(3, 102, 2, 70),
                        new Lieferung(3, 142, 1, 50),
                        new Lieferung(1, 123, 1, 35),
                        new Lieferung(5, 123, 3, 40),
                        new Lieferung(1, 102, 2, 90),
                        new Lieferung(1, 114, 1, 50),
                        new Lieferung(2, 114, 1, 100),
                        new Lieferung(2, 112, 4, 60),
                        new Lieferung(3, 132, 2, 10),
                        new Lieferung(5, 124, 1, 80),
                        new Lieferung(3, 102, 1, 68),
                        new Lieferung(3, 143, 3, 70),
                        new Lieferung(2, 112, 2, 50),
                        new Lieferung(1, 102, 1, 50),
                        new Lieferung(2, 134, 1, 20),
                        new Lieferung(3, 141, 3, 70),
                        new Lieferung(5, 115, 2, 10),
                        new Lieferung(3, 111, 1, 50),
                        new Lieferung(5, 103, 1, 80),
                        new Lieferung(1, 112, 3, 50),
                        new Lieferung(2, 131, 2, 100),
                        new Lieferung(1, 133, 1, 60),
                        new Lieferung(3, 143, 1, 60),
                        new Lieferung(3, 141, 3, 40),
                        new Lieferung(5, 102, 2, 35),
                        new Lieferung(1, 112, 1, 55),
                        new Lieferung(2, 114, 1, 40),
                        new Lieferung(2, 116, 3, 90)
                ), "LNr, TNr, PNr, Menge");

        zubehoer = tEnv.fromDataSet(
                env.fromElements(
                        new Zubehoer(101, "Schraube A", "Grau", 3),
                        new Zubehoer(102, "Schraube A", "Schwarz", 3),
                        new Zubehoer(103, "Schraube B", "Grau", 4),
                        new Zubehoer(104, "Schraube B", "Schwarz", 4),
                        new Zubehoer(111, "Mutter A", "Grau", 3),
                        new Zubehoer(112, "Mutter A", "Silber", 3),
                        new Zubehoer(113, "Mutter A", "Schwarz", 3),
                        new Zubehoer(114, "Mutter A", "Blau", 3),
                        new Zubehoer(115, "Mutter B", "Grau", 12),
                        new Zubehoer(116, "Mutter B", "Silber", 12),
                        new Zubehoer(117, "Mutter B", "Schwarz", 12),
                        new Zubehoer(118, "Mutter B", "Blau", 12)
                ), "TNr, TName, Farbe, Gewicht");
    }

}