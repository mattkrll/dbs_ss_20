package flinktableapi.UserDataTypes;

/**
 * Einfaches POJO zum modellieren von Zubehoer
 */

public class Zubehoer{
    public long TNr;
    public String TName;
    public String Farbe;
    public long Gewicht;

    public Zubehoer() {}

    public Zubehoer(long TNr, String TName, String Farbe, long Gewicht) {
        this.TNr = TNr;
        this.TName = TName;
        this.Farbe = Farbe;
        this.Gewicht = Gewicht;
    }

    @Override
    public String toString() {
        return "Zubehoer{" +
                "TNr=" + TNr +
                ", TName='" + TName + '\'' +
                ", Farbe='" + Farbe + '\'' +
                ", Gewicht=" + Gewicht +
                '}';
    }
}
